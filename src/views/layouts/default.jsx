import React from "react";
import { Col, Row } from "react-bootstrap";
import { Components } from "../../components";

export const Layout = (props) => {
  return (
    <>
      <main className="main container">
        <Row>
          <Components.Navbar />
          <Col className="py-5">{props.children}</Col>
        </Row>
      </main>
    </>
  );
};
