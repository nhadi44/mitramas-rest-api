import React, { useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  addCustomer,
  deleteCustomer,
  getCustomers,
  updateCustomer,
} from "../../features/customers/customerSlice";

import DataTable from "react-data-table-component";
import { Layout } from "../layouts/default";
import { FilterComponent } from "../../components/filter";
import { Alert, Button, Form, Modal } from "react-bootstrap";
import { Success } from "../../components/alerts/success";

export const Dashboard = () => {
  const [textFilter, setTextFilter] = useState("");
  const [resetPaginationToggle, setResetPaginationToggle] = useState(false);
  const [editModal, setEditModal] = useState(false);
  const [createModal, setCreateModal] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [dataModal, setDataModal] = useState({
    id: "",
    name: "",
    address: "",
    country: "",
    phone: "",
    job_title: "",
    status: "",
  });

  const token = localStorage.getItem("token");

  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    let time = 60 * 60 * 1000;
    const timer = setInterval(() => {
      time--;
      if (time === 0) {
        clearInterval(timer);
        localStorage.removeItem("token");
        navigate("/login");
      }
    }, 1000);
  }, []);

  useEffect(() => {
    setTimeout(() => {
      dispatch(getCustomers());
    }, 1000);
  }, [dispatch]);

  const data = useSelector((state) => state.customers.entity);
  const loading = useSelector((state) => state.customers.loading);
  const error = useSelector((state) => state.customers.error);
  const success = useSelector((state) => state.customers.success);
  const message = useSelector((state) => state.customers.message);

  const filterItems = useMemo(() => data, [data]);
  const filteredData = useMemo(() => {
    return filterItems
      .filter((item) => {
        return item.name.toLowerCase().includes(textFilter.toLowerCase());
      })
      .sort((a, b) => {
        return a.name.localeCompare(b.name);
      });
  }, [filterItems, textFilter]);

  // const filteredData = useMemo(() => filterItems, [filterItems, textFilter]);
  const subHeaderComponentMemo = useMemo(() => {
    const handleClear = () => {
      if (textFilter) {
        setResetPaginationToggle(!resetPaginationToggle);
        setTextFilter("");
      }
    };

    return (
      <>
        <FilterComponent
          onFilter={(e) => setTextFilter(e.target.value)}
          onClear={handleClear}
          filterText={textFilter}
        />
      </>
    );
  }, [textFilter, resetPaginationToggle]);

  useEffect(() => {
    if (!token) {
      navigate("/login");
    }
  }, [token, navigate]);

  const columns = [
    {
      name: "Name",
      selector: (row) => row.name,
      sortable: true,
    },
    {
      name: "Address",
      selector: (row) => row.address,
      sortable: true,
    },
    {
      name: "Country",
      selector: (row) => row.country,
      sortable: true,
    },
    {
      name: "Phone",
      selector: (row) => row.phone_number,
      sortable: true,
    },
    {
      name: "Job Title",
      selector: (row) => row.job_title,
      sortable: true,
    },
    {
      name: "Status",
      selector: (row) => row.status,
      sortable: true,
      cell: (row) => <div>{row.status ? "Active" : "Inactive"}</div>,
    },
    {
      name: "Created At",
      selector: (row) => row.created_at,
      sortable: true,
    },
    {
      name: "Updated At",
      selector: (row) => row.updated_at,
      sortable: true,
    },
    {
      name: "Action",
      selector: (row) => row.id,
      cell: (row) => (
        <>
          <div className="d-flex gap-2">
            <Button variant="primary" onClick={() => showEdit(row)}>
              <i className="bi bi-pencil-square"></i>
            </Button>
            <Button variant="danger" onClick={() => showDelete(row)}>
              <i className="bi bi-trash-fill"></i>
            </Button>
          </div>
        </>
      ),
    },
  ];

  const showEdit = (id) => {
    setDataModal({
      id: id.id,
      name: id.name,
      address: id.address,
      country: id.country,
      phone: id.phone_number,
      job_title: id.job_title,
      status: id.status,
    });

    setEditModal(true);
  };

  const showCreate = () => {
    setCreateModal(true);
  };

  const showDelete = (e) => {
    setDataModal({
      id: e.id,
      name: e.name,
    });

    setDeleteModal(true);
  };

  const closeEdit = () => setEditModal(false);
  const closeCreate = () => setCreateModal(false);
  const closeDelete = () => setDeleteModal(false);
  const [show, setShow] = useState(true);
  return (
    <>
      <Layout>
        <Button variant="success" className="px-3" onClick={showCreate}>
          Add Customer
        </Button>
        <DataTable
          title="Customer List"
          columns={columns}
          data={filteredData}
          pagination
          paginationResetDefaultPage={resetPaginationToggle} // optionally, a hook to reset pagination to page 1
          subHeader
          subHeaderComponent={subHeaderComponentMemo}
          progressPending={loading}
          persistTableHead
        />
        <span>{error ? "Error" : "No error"}</span>
      </Layout>
      {/* Modal Update */}
      <Modal show={editModal} onHide={closeEdit}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Customer</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group controlId="formBasicName" className="mb-3">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter name"
                value={dataModal.name}
                onChange={(e) =>
                  setDataModal({ ...dataModal, name: e.target.value })
                }
                autoFocus
              ></Form.Control>
            </Form.Group>
            <Form.Group controlId="formBasicAddress" className="mb-3">
              <Form.Label>Address</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter address"
                value={dataModal.address}
                onChange={(e) =>
                  setDataModal({ ...dataModal, address: e.target.value })
                }
              ></Form.Control>
            </Form.Group>
            <Form.Group controlId="formBasicCountry" className="mb-3">
              <Form.Label>Country</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Country"
                value={dataModal.country}
                onChange={(e) =>
                  setDataModal({ ...dataModal, country: e.target.value })
                }
              ></Form.Control>
            </Form.Group>
            <Form.Group controlId="formBasicPhone" className="mb-3">
              <Form.Label>Phone Number</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Phone Number"
                value={dataModal.phone}
                onChange={(e) =>
                  setDataModal({ ...dataModal, phone: e.target.value })
                }
              ></Form.Control>
            </Form.Group>
            <Form.Group controlId="formBasicJob" className="mb-3">
              <Form.Label>Job Title</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Job Title"
                value={dataModal.job_title}
                onChange={(e) =>
                  setDataModal({ ...dataModal, job_title: e.target.value })
                }
              ></Form.Control>
            </Form.Group>
            <Form.Group controlId="formBasicStatus" className="mb-3">
              <Form.Label>Status</Form.Label>
              <Form.Select
                value={dataModal.status}
                onChange={(e) => {
                  setDataModal({ ...dataModal, status: e.target.value });
                }}
              >
                <option value={true}>Active</option>
                <option value={false}>Inactive</option>
              </Form.Select>
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={closeEdit}>
            Close
          </Button>
          <Button
            variant="primary"
            onClick={async (e) => {
              await dispatch(updateCustomer({ ...dataModal }));
              await dispatch(getCustomers());
              closeEdit();
            }}
          >
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
      {/* Modal Update End */}
      {/* Modal Add */}
      <Modal show={createModal} onHide={closeCreate}>
        <Modal.Header closeButton>
          <Modal.Title>Add Customer</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group controlId="formBasicName" className="mb-3">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter name"
                onChange={(e) =>
                  setDataModal({ ...dataModal, name: e.target.value })
                }
                autoFocus
              ></Form.Control>
            </Form.Group>
            <Form.Group controlId="formBasicAddress" className="mb-3">
              <Form.Label>Address</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter address"
                onChange={(e) =>
                  setDataModal({ ...dataModal, address: e.target.value })
                }
              ></Form.Control>
            </Form.Group>
            <Form.Group controlId="formBasicCountry" className="mb-3">
              <Form.Label>Country</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Country"
                onChange={(e) =>
                  setDataModal({ ...dataModal, country: e.target.value })
                }
              ></Form.Control>
            </Form.Group>
            <Form.Group controlId="formBasicPhone" className="mb-3">
              <Form.Label>Phone Number</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Phone Number"
                onChange={(e) =>
                  setDataModal({ ...dataModal, phone: e.target.value })
                }
              ></Form.Control>
            </Form.Group>
            <Form.Group controlId="formBasicJob" className="mb-3">
              <Form.Label>Job Title</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Job Title"
                onChange={(e) =>
                  setDataModal({ ...dataModal, job_title: e.target.value })
                }
              ></Form.Control>
            </Form.Group>
            <Form.Group controlId="formBasicStatus" className="mb-3">
              <Form.Label>Status</Form.Label>
              <Form.Select
                onChange={(e) => {
                  setDataModal({ ...dataModal, status: e.target.value });
                }}
                defaultValue={"null"}
              >
                <option value={"null"} disabled>
                  Select Status
                </option>
                <option value={true}>Active</option>
                <option value={false}>Inactive</option>
              </Form.Select>
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={closeCreate}>
            Close
          </Button>
          <Button
            variant="success"
            onClick={async () => {
              await dispatch(addCustomer({ ...dataModal }));
              dispatch(getCustomers());
              closeCreate();
            }}
          >
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
      {/* Modal Add End */}
      {/* Delete Modal */}
      <Modal show={deleteModal} onHide={closeDelete}>
        <Modal.Header closeButton>
          <Modal.Title>Delete Customer</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            Are you sure you want to delete <strong>{dataModal.name}</strong> ?
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={closeDelete}>
            Close
          </Button>
          <Button
            variant="danger"
            onClick={async () => {
              await dispatch(deleteCustomer({ ...dataModal }));
              await dispatch(getCustomers());
              closeDelete();
            }}
          >
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
      {/* Delete Modal End */}
    </>
  );
};
