import React, { useEffect, useState } from "react";
import Form from "react-bootstrap/Form";
import { Container, Spinner } from "react-bootstrap";
import { Components } from "../../components";
import "../../assets/css/views/login.css";
import { Button } from "react-bootstrap";
import { AuthLogin } from "../../features/auth/loginSlice";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

export const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailValid, setEmailValid] = useState("");
  const [passwordValid, setPasswordValid] = useState("");

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const entity = useSelector((state) => state.login.entity);
  const loading = useSelector((state) => state.login.isLoading);

  const token = localStorage.getItem("token");

  const loginUser = async (e) => {
    e.preventDefault();
    if (!email) {
      setEmailValid("Email is required");
    } else if (!password) {
      setPasswordValid("Password is required");
    } else {
      await dispatch(AuthLogin({ email, password }));
      navigate("/");
    }
  };

  useEffect(() => {
    if (token) return navigate("/");
  }, [token, navigate]);

  return (
    <>
      <div className="login bg-primary">
        <Container className="vh-100 d-flex align-items-center justify-content-center w-100">
          <div className="form__wrapper">
            <Components.Login>
              <Form onSubmit={loginUser}>
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="email"
                    onChange={(e) => setEmail(e.target.value)}
                  />
                  <small className="text-danger">{emailValid}</small>
                </Form.Group>
                <Form.Group controlId="formBasicPassword" className="mb-3">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    onChange={(e) => setPassword(e.target.value)}
                  />
                  <small className="text-danger">{passwordValid}</small>
                </Form.Group>
                <Form.Group>
                  {loading ? (
                    <Button type="submit" className="w-100">
                      <Spinner animation="border" variant="light" />
                    </Button>
                  ) : (
                    <Button type="submit" className="w-100">
                      Login
                    </Button>
                  )}
                </Form.Group>
              </Form>
            </Components.Login>
          </div>
        </Container>
      </div>
    </>
  );
};
