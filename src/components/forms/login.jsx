import React from "react";
import Card from "react-bootstrap/Card";
import { Row, Col } from "react-bootstrap";

export const Login = (props) => {
  return (
    <>
      <Row>
        <Card>
          <Card.Body>
            <Card.Title className="text-center fs-2 text-uppercase mb-4">
              Login
            </Card.Title>
            <div>{props.children}</div>
          </Card.Body>
        </Card>
      </Row>
    </>
  );
};
