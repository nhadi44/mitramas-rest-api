import React from "react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export const Navbar = () => {
  const logout = () => {
    localStorage.removeItem("token");
    window.location.href = "/login";
  };
  return (
    <>
      <nav className="navbar">
        <Link to="/" className="nav-link fs-4">
          Mitramas Rest API
        </Link>
        <Button variant="danger" onClick={logout}>
          Logout
        </Button>
      </nav>
    </>
  );
};
