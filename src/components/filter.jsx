export const FilterComponent = ({ filterText, onFilter, onClear }) => {
  return (
    <div className="form-group">
      <input
        type="text"
        className="form-control"
        placeholder="Search"
        value={filterText}
        onChange={onFilter}
      />
    </div>
  );
};
