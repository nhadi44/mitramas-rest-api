import { Login } from "./forms/login";
import { Navbar } from "./navbar";

export const Components = {
  Login,
  Navbar,
};
