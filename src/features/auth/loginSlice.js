import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const AuthLogin = createAsyncThunk(
  "login/AuthLogin",
  async ({ email, password }) => {
    const response = await axios
      .post("https://mitramas-test.herokuapp.com/auth/login", {
        email: email,
        password: password,
      })
      .then((res) => {
        localStorage.setItem("token", res.data.access_token);
      });
    return response.data;
  }
);

const initialSate = {
  isLoading: false,
  token: "",
  entity: {},
};

const loginSlice = createSlice({
  name: "login",
  initialState: initialSate,
  extraReducers: (builder) => {
    builder.addCase(AuthLogin.pending, (state, action) => {
      state.isLoading = true;
    });
    builder.addCase(AuthLogin.fulfilled, (state, action) => {
      state.isLoading = false;
      state.token = action.payload.access_token;
      state.entity = action.payload;
    });
  },
});

export default loginSlice.reducer;
