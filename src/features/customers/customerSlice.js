import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const getCustomers = createAsyncThunk(
  "customers/getCustomers",
  async () => {
    const response = await axios({
      method: "get",
      url: "https://mitramas-test.herokuapp.com/customers",
      headers: {
        Authorization: localStorage.getItem("token"),
      },
    });
    return response.data;
  }
);

export const updateCustomer = createAsyncThunk(
  "customers/updateCustomer",
  async (dataModal) => {
    const response = await axios({
      method: "put",
      url: "https://mitramas-test.herokuapp.com/customers",
      headers: {
        Authorization: localStorage.getItem("token"),
      },
      data: {
        id: dataModal.id,
        name: dataModal.name,
        address: dataModal.address,
        country: dataModal.country,
        phone_number: dataModal.phone,
        job_title: dataModal.job_title,
        status: dataModal.status,
      },
    }).then((e) => {
      getCustomers();
    });
    return response.data;
  }
);

export const addCustomer = createAsyncThunk(
  "customers/addCustomer",
  async (dataModal) => {
    const response = await axios({
      method: "post",
      url: "https://mitramas-test.herokuapp.com/customers",
      headers: {
        Authorization: localStorage.getItem("token"),
      },
      data: {
        name: dataModal.name,
        address: dataModal.address,
        country: dataModal.country,
        phone_number: dataModal.phone,
        job_title: dataModal.job_title,
        status: dataModal.status,
      },
    }).then((e) => {
      getCustomers();
    });
    return response.data;
  }
);

export const deleteCustomer = createAsyncThunk(
  "customers/deleteCustomer",
  async (dataModal) => {
    const response = await axios({
      method: "delete",
      url: "https://mitramas-test.herokuapp.com/customers",
      headers: {
        Authorization: localStorage.getItem("token"),
      },
      data: {
        id: dataModal.id,
      },
    }).then(() => {
      getCustomers();
    });
    return response.data;
  }
);

const initialState = {
  entity: [],
  loading: false,
  error: null,
  message: null,
  success: false,
};

const customerSlice = createSlice({
  name: "customers",
  initialState: initialState,
  extraReducers: (builder) => {
    builder.addCase(getCustomers.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(getCustomers.fulfilled, (state, action) => {
      state.loading = false;
      state.entity = action.payload.data;
    });
    builder.addCase(getCustomers.rejected, (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    });

    builder.addCase(updateCustomer.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(updateCustomer.fulfilled, (state, action) => {
      state.loading = false;
      state.entity = action.payload.data;
      state.success = action.payload.data.success;
      state.message = "Update Successfully";
    });
    builder.addCase(updateCustomer.rejected, (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    });

    builder.addCase(addCustomer.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(addCustomer.fulfilled, (state, action) => {
      state.loading = false;
      state.entity = action.payload.data;
    });
    builder.addCase(addCustomer.rejected, (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    });

    builder.addCase(deleteCustomer.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(deleteCustomer.fulfilled, (state, action) => {
      state.loading = false;
      state.entity = action.payload.data;
    });
    builder.addCase(deleteCustomer.rejected, (state, action) => {
      state.loading = false;
      state.error = action.error.message;
    });
  },
});

export default customerSlice.reducer;
