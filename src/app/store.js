import { configureStore } from "@reduxjs/toolkit";
import loginReducer from "../features/auth/loginSlice";
import customerReducer from "../features/customers/customerSlice";

export const store = configureStore({
  reducer: {
    login: loginReducer,
    customers: customerReducer,
  },
});
