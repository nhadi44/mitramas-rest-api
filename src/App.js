import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { ViewAuth } from "./views/auth";
import { ViewDashboard } from "./views/dashboard";
import { Error } from "./views/error/404";

export const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<ViewDashboard.Dashboard />} />
        <Route path="/login" element={<ViewAuth.Login />} />

        <Route path="*" element={<Error />} />
      </Routes>
    </BrowserRouter>
  );
};
